# Propuesta de division de los json de configuracion

## Descripcion

En las aplicaciones CELLS medianas y grandes se generan ficheros JSON de configuracion demasiado grandes, lo que dificulta la mantenibilidad de los mismos.

Se propone utilizar una estructura alternativa que permita agrupar elementos comunes y que permita un uso mas racional del que actualmente tenemos.

## cosas que se hacen actualmente en el JSON de configuracion:

- se definen que componentes se van a poner en la pagina
- se definen que eventos escucha y emiten los componentes
- se definen los parametros que se les pasan a los componentes
- se definen las rutas desde las que se va a navegar a la pagina 

## tipos de elementos en el JSON de configuración

En el JSON de configuracion se pueden encontrar 

- elementos visuales
- controladores
- datamanagers
- cross componets 

## Estrategia propuesta 

Permitir definir los actuales elementos en ficheros separados cuando los usuarios lo necesiten 

Permitir tener los cross components en un fichero separados para definirlos una unica vez para toda la aplicacion 


Permitir tener varios datamanagers o controllers definidos una unica vez 

tener un fichero de rutas en el que se especifique las paginas que se puede navegar en la aplicacion

Separar la definicion de la construccion de la inyeccion de los eventos 

## ejemplo cuando la misma configuracion para un dm esta en varios json de pagina 

en la pagina de cheques y de tdc se utiliza el mismo dm dm-user-info del componente datamanagers-global-mx, por lo que en el fichero ./datamanagers/datamanagers-global-mx.json incluimos una clave 
```javascript
{
   "dm-user-info":{
      ....
	},
}
```

con el contenido comun 
```javascript
      "zone": "app__main",
      "type": "DM",
      "tag": "dm-user-info",
      "familyPath": "../components/datamanagers-global-mx",
      "properties": {
        "environment": "",
        "cellsConnections": {
          "in": {
            "granting-ticket-channel": {
              "bind": "setTsec"
            }
          },
          "out": {
            "set_user_info": {
              "bind": "user-info-changed"
            },
            "service-error-channel": {
              "bind": "error-event"
            }
          }
        }
      }
```

y en las paginas que lo utilizan
```javascript
    {
      "componentDefinitionUrl": "./datamanagers/datamanagers-global-mx",
      "key": "dm-user-info",
    },
```

de forma que solo tenemos el codigo una unica vez y lo referenciamos en multiples paginas, si hay que hacer un cambio para todos solo lo tocamos una vez.



## ejemplo cuando tenemos diferentes definiciones para el mismo dm en diferentes paginas (los parametros cambian )

supongamos que tenemos el componente datamanagers-mx-new que dentro tiene varios dm pero uno de ellos tiene diferentes configuraciones dependiendo de la pagina en la que se utiliza

en la pagina de cheques la configuracion es 

```javascript
{
      "zone": "app__main",
      "type": "DM",
      "tag": "dm-tabs",
      "familyPath": "../components/datamanagers-mx-new",
      "properties": {
        "environment": "",
        "cellsConnections": {
          "in": {},
          "out": {
            "set_tabs": {
              "bind": "tabs-changed"
            },
            "service-error-channel": {
              "bind": "error-event"
            }
          },
          "params": {
            "account-type": "accountType"
          }
        }
      }
    }
```

y en la pagina de tdc es 

```javascript
{
      "zone": "app__main",
      "type": "DM",
      "tag": "dm-tabs",
      "familyPath": "../components/datamanagers-mx-new",
      "properties": {
        "environment": "",
        "cellsConnections": {
          "in": {},
          "out": {
            "set_tabs": {
              "bind": "tabs-changed"
            },
            "service-error-channel": {
              "bind": "error-event"
            },
            "set_movements-response": {
              "bind": "movements-response-changed"
            }
          }
        }
      }
    }
```

es una importacion del mismo componente con diferentes configuraciones, pues en la carpeta datamanagers, se crea el fichero datamanagers-mx-new.json y dentro ponemos una lista de objetos con clave 
```javascript
{
	"dm-tabs":{
      ...
      "tag": "dm-tabs",
      ...
    },
    "dm-tabs-checks":{
      ...
      "tag": "dm-tabs",
      ...
    }
}
```

y en cada uno de los json de cheques y tdc se pone una referencia

para cheques 

```javascript
    {
      "componentDefinitionUrl": "./datamanagers/datamanagers-mx-new",
      "key": "dm-tabs-checks",
    },
```

para tdc 

```javascript
    {
      "componentDefinitionUrl": "./datamanagers/datamanagers-mx-new",
      "key": "dm-tabs",
    },
```

De esta forma podemos tener diferentes configuraciones para el mismo datamanager
