# Propuesta de simplificacion de los canales de comunicacion 

## Descripcion
La disposicion actual de la informacion de sobre los canales de comunicacion separados de la definicion de los componentes lleva a ficheros de configuracion muy grandes.

Se propone crear una forma alternativa de definir que datos se pasan a los componentes y como se gestionan los eventos que disparan para tener algo mas proximo a lo que hacen en otros frameworks

La propuesta se basa en un modo convivencia, en el que la version anterior y La propuesta puedan utilizarse indistintamente

## cambios en la definicion de como se indican los canales de entrada

Los canales de entrada es la forma que tiene cells de indicar que valores de Los diferentes componentes son cambiados cuando se dispara un evento.

El json de configuracion actual es 
```javascript
{
      "zone": "app__main",
      "type": "CC",
      "familyPath": "../components/cells-service-error-handler",
      "tag": "cells-service-error-handler",
      "properties": {
        "cellsConnections": {
          "in": {
            "service-error-channel": {
              "bind": "handleError"
            }
          },
          "out": {}
        }
      }
    },
``` 

Si fuese un componente "normal de polymer" lo hariamos asi
```javascript
<cells-service-error-handler
    handleError ="[[service-error-channel]]"
>
	
</cells-service-error-handler>
```

Donde la propiedad del compoente "handleError" se bindea a lo que traiga como parametro el canal "service-error-channel"

Trasladado al modelo propuesto la definicion en el json quedaria 
```javascript
    {
      "zone": "app__main",
      "type": "CC",
      "familyPath": "../components/cells-service-error-handler",
      "tag": "cells-service-error-handler",
      "properties": {
        "handleError": "[[service-error-channel]]"
      }
    },
```

De forma que se elimina toda la estructura innecesaria de propiedades y subpropiedades, simplemente las propiedades son "bindeadas"

En caso de que quisiesemos poner un valor de una propiedad que no es bindeada se pondria:

```javascript
    {
      "zone": "app__main",
      "type": "CC",
      "familyPath": "../components/cells-service-error-handler",
      "tag": "cells-service-error-handler",
      "properties": {
        "handleError": "[[service-error-channel]]",
        "otraPropiedad":"Valor sin bindeo"
      }
    },
```

Existen actualmente dos tipos de bindeos que se hacen a componentes, llamar a una funcion del componente o cambiar el valor de una propiedad, ambos se realizarian de la misma manera, siendo cells-brigde el que miraria si es propiedad o funcion y llamandola o asignandola segun aplica.

Importante: la funcionalidad no cambia, solamente se tiene otra forma de definir el canal

Esta estrategia facilita realizar otro tipo de funcionalidades como 
```javascript
    {
      "zone": "app__main",
      "type": "UI",
      "familyPath": "../components/cells-componente",
      "tag": "cells-componente",
      "properties": {
        "name": "[[usuario-channel.title]]",
        "description": "[[usuario-channel.extendedInfo]]",
        "image": "[[usuario-channel.profileImage]]"
      }
    },
```

En la que las propiedades del objeto enviado como parametro en el canal son asignadas 1 a 1 de forma que no es necesario generar objetos intermedios ni crear un metodo para incializar propiedades a partir de un objeto.

Esta estrategia tambien permite especificar si las propiedades son [[]] o {{}}

## Cambios en la definicion de como se indican los canales de salida

Los canales de salida son una forma que tiene cells de escuchar los fire que se disparan en los componentes que estan a primer nivel en la aplicacion para despues cambiar las porpiedades o llamar a las funciones de los otros componentes que tienen definidos in en el mismo canal.

Adicionalmente las propiedades que tienen notify:true y que se definen como "propiedad-changed" se hace un fire al canal con el valor de la propiedad cuando esta cambia (pone un observer).

En la mayoria de los frameworks actuales no es necesario definir explicitamente en ningun sitio que eventos dispara cada componente, se escuchan todos los eventos que se producen y si hay alguien subscrito a ellos se pasa el valor o se llama a la funcion.

Tambien se intenta que no importe en que nivel de anidacion este el componente que dispara, para eso se utiliza el bubling.  

Como las aplicaciones actuales estan "pensadas" para funcionar asi, se propone una forma alternativa de eliminar para aquellos que lo deseen puedan no mantener una estructura fuertemente acoplada y de mantenimiento manual que es a dia de hoy la seccion de out.


Proponemos utilizar la forma de Polymer 2.0 de gestionar los eventos custom
https://www.polymer-project.org/2.0/docs/devguide/events

```javascript
this.dispatchEvent(
      new CustomEvent(
                   'service-error-channel', 
                    {
                      detail: {
                                type:"service-error-event",
                                service: 'cuentas', 
                                code: 500,
                                errorDescription:"Error al obtener las cuentas del usuario"
                      },
                      bubbles: true, 
                      composed: true
                    }));
```

De forma que los eventos disparados de esta manera en los componentes no sea necesario registrarlo en los out, simplemente en el bridge se mira quien esta subcrito y se actualizan sus propiedades o se llama a los metodos correspondientes.

Si alguien encuentra mas comodo tener como referencia los out con esta nueva manera de disparar los eventos, puede hacerlo tambien, pero definirdos o no, no existira diferencia. 

Si el composer necesita definir los out, como es opcional tambien puede hacerse.

Nota: fijarse que el canal va en el primer parametro del custom event y se puede utilizar "type" dentro del detalle para que en un mismo canal viajen diferentes tipos de mensajes

Nota2: fijarse que se han incluido las propiedades buble y composed como se recomienda en la documentacion de polymer para que estos eventos no "mueran" al llegar al limite de un shadow dom

Me pregunta un developer ¿donde puedo ver que eventos dispara un componente?
La respuesta a esto es sencilla, en la documentacion se definen el contrato del componente.

Nota tecnica:En la documetacion de polymer se indica que la forma de escuchar un evento custom es:
  document.querySelector('x-custom').addEventListener('kick', function (e) {
        console.log(e.detail.kicked); // true
    })

## que hacer con los links 

Para hacer que un evento disparado con fire haga una navegacion, se pone  como evento de out algo como:
```javascript
	"movements-channel": {
	  "bind": "home-e",
	  "link": {
	    "page": "checkAccount",
	    "params": {
	      "idCard": "accountId"
	    }
	  }
	 }

```

 En el nuevo modelo bastaria con poner el link como propiedad del evento 
```javascript 
this.dispatchEvent(
      new CustomEvent(
                   'movements-channel', 
                    {
                      detail: {type:'home-e'},
                  	  'link': {
						    'page': 'checkAccount',
						    'params': {
						      'idCard': 'accountId'
						   }
					  },
					  bubbles: true, 
                      composed: true
                    }));
```

## ejemplo completo 

Un datamanager actual 

```javascript
{
      "zone": "app__main",
      "type": "DM",
      "tag": "dm-accounts-send-by-email",
      "familyPath": "../components/datamanagers-accounts-mx",
      "properties": {
        "environment": "",
        "cellsConnections": {
          "in": {
            "send-email-with-password-data-channel": {
              "bind": "prepareToSend"
            },
             "period-id-selected-event-channel":{
                "bind":"_setPeriod"
            },
            "send-token-event-channel": {
              "bind": "_confirmToken"
            },
            "cancel-token-channel": {
              "bind": "resetModal"
            }
          },
          "out": {
            "set_email": {
              "bind": "email-changed"
            },
            "send_account-successful-channel": {
              "bind": "email-sent-successful"
            },
            "service-error-channel": {
              "bind": "error-event"
            },
            "email-data-saved-channel":{
              "bind":"email-data-saved"
            },
            "checkTokenModal-channel":{
              "bind":"check-token-modal-changed"
            },
            "token-error-channel":{
              "bind":"token-error-changed"
            },
            "send-error-email": {
              "bind": "error-message-email-changed"
            },
            "set-loading": {
              "bind":"loading-changed"
            }

            
          },
          "params": {
            "accountId": "accountId"
          }
        }
      }
    }
```


Pasaria a ser 
```javascript
"zone": "app__main",
      "type": "DM",
      "tag": "dm-accounts-send-by-email",
      "familyPath": "../components/datamanagers-accounts-mx",
      "properties": {
        "environment": "",
        "prepareToSend":"send-email-with-password-data-channel",
        "_setPeriod":"period-id-selected-event-channel",
        "_confirmToken":"send-token-event-channel",
        "resetModal":"cancel-token-channel",

        "cellsConnections": {
          "params": {
            "accountId": "accountId"
          }
        }
      }
    }
```

Se podria tambien mover la parte de los parametros y de las rutas para eliminar el cellsConnections totalmente.